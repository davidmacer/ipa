db_get_table <- function(conn, statement) {
  df <- dbGetQuery(conn = conn, statement = statement) %>% as_tibble()
  names(df) <- tolower(names(df))
  if("geom" %in% colnames(df)){
    df$geom <- sf:::st_as_sfc.WKB(df$geom, EWKB = TRUE)
    df <- st_as_sf(df)
    return(df)
  }else{
    return(df)
  }
}